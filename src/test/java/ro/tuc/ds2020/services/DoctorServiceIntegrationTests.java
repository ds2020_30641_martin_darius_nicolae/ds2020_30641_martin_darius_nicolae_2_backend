package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Caregiver;

import static org.springframework.test.util.AssertionErrors.assertEquals;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class DoctorServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    DoctorService doctorService;

    @Test
    public void testGetCorrect() {
        List<DoctorDTO> doctorDTOList = doctorService.findDoctors();
        assertEquals("Test Insert Caregiver", 2, doctorDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        DoctorDTO p = new DoctorDTO("Ioan","male","doctor@yahoo.com","pass");
        UUID insertedID = doctorService.insert(p);

        DoctorDTO insertedMedication = new DoctorDTO(insertedID, p.getName(),p.getGender(),p.getEmail(),p.getPassword());
        DoctorDTO fetchedMedication = doctorService.findDoctorById(insertedID);

        assertEquals("Test Inserted Medication", insertedMedication, fetchedMedication);
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        DoctorDTO p = new DoctorDTO("Ioan","male","doctor@yahoo.com","pass");
        doctorService.insert(p);

        List<DoctorDTO> caregiverDTOList = doctorService.findDoctors();
        assertEquals("Test Inserted Caregiver", 1, caregiverDTOList.size());
    }
}
