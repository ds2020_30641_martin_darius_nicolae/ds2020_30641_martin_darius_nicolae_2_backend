package ro.tuc.ds2020.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.services.PersonService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class MedicationControllerUnitTest extends Ds2020TestConfig {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService service;

    @Test
    public void insertMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDTO medicationDTO = new MedicationDTO("tusin", "durere de burta", "22");

        mockMvc.perform(post("/medication/insert")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }


}
