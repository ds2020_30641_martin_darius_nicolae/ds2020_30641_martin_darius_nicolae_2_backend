package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;

import java.util.ArrayList;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        PatientDTO patientDTO=null;
        ArrayList<MedicationDTO> medicationDTO=new ArrayList<MedicationDTO>();
        if(medicationPlan.getListOfMedications()!=null){
            for(int i=0;i<medicationPlan.getListOfMedications().size();i++){
                Medication med=medicationPlan.getListOfMedications().get(i);
                MedicationDTO medDTO=MedicationBuilder.toMedicationDTO(med);
                medicationDTO.add(medDTO);
            }
        }
        if(medicationPlan.getPatient()!=null) {
            patientDTO=PatientBuilder.toPatientDTO(medicationPlan.getPatient());
        }
        return new MedicationPlanDTO(medicationPlan.getId_medicationPlan(),medicationPlan.getName(),medicationPlan.getIntakeIntervals(),medicationPlan.getPeriod(),medicationDTO,patientDTO);
    }


    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        Patient patient=null;
        ArrayList<Medication> medications=new ArrayList<Medication>();
        if(medicationPlanDTO.getListOfMedications()!=null){
            for(int i=0;i<medicationPlanDTO.getListOfMedications().size();i++) {
                MedicationDTO medDTO = medicationPlanDTO.getListOfMedications().get(i);
                Medication med=MedicationBuilder.transform(medDTO);
                medications.add(med);
            }
        }
        if(medicationPlanDTO.getPatientDTO()!=null){
            patient=PatientBuilder.transform(medicationPlanDTO.getPatientDTO());
        }
        return new MedicationPlan(medicationPlanDTO.getName(),medicationPlanDTO.getIntakeIntervals(),medicationPlanDTO.getPeriod(),medications,patient);
    }

    public static MedicationPlan transform(MedicationPlanDTO medicationPlanDTO) {
        Patient patient=null;
        ArrayList<Medication> medications=new ArrayList<Medication>();
        if(medicationPlanDTO.getListOfMedications()!=null){
            for(int i=0;i<medicationPlanDTO.getListOfMedications().size();i++) {
                MedicationDTO medDTO = medicationPlanDTO.getListOfMedications().get(i);
                Medication med=MedicationBuilder.transform(medDTO);
                medications.add(med);
            }
        }
        if(medicationPlanDTO.getPatientDTO()!=null){
            patient=PatientBuilder.transform(medicationPlanDTO.getPatientDTO());
        }
        return new MedicationPlan(medicationPlanDTO.getId_medicationPlan(),medicationPlanDTO.getName(),medicationPlanDTO.getIntakeIntervals(),medicationPlanDTO.getPeriod(),medications,patient);
    }
}


