package ro.tuc.ds2020.dtos;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.hateoas.RepresentationModel;


import java.util.Objects;
import java.util.UUID;

public class DoctorDTO extends RepresentationModel<DoctorDTO>  {

    private UUID idDoctor;
    private String name;
    private String gender;
    private String email;
    private String password;

    public DoctorDTO(){

    }

    public DoctorDTO(UUID idDoctor, String name, String gender, String email, String password) {
        this.idDoctor = idDoctor;
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }

    public DoctorDTO(String name, String gender, String email, String password) {
        this.name = name;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }

    public UUID getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(UUID idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DoctorDTO doctorDTO = (DoctorDTO) o;
        return idDoctor.equals(doctorDTO.idDoctor) &&
                name.equals(doctorDTO.name) &&
                gender.equals(doctorDTO.gender) &&
                email.equals(doctorDTO.email) &&
                password.equals(doctorDTO.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idDoctor, name, gender, email, password);
    }
}
