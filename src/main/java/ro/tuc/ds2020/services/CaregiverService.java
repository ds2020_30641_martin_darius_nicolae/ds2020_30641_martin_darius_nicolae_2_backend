package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if (!caregiver.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(caregiver.get());
    }

    public CaregiverDTO findCaregiverByName(String name) {
        List<Caregiver> list = caregiverRepository.findByName(name);
        if (list.isEmpty()) {
            LOGGER.error("Caregiver with name {} was not found in db", name);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with name: " + name);
        }
        return CaregiverBuilder.toCaregiverDTO(list.get(0));
    }

    public UUID insert(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public UUID delete(String name){
        List<Caregiver>  listOfCaregivers=caregiverRepository.findByName(name);
        caregiverRepository.delete(listOfCaregivers.get(0));
        return listOfCaregivers.get(0).getId();
    }

    public UUID update(CaregiverDTO updatedCaregiver){
        Caregiver caregiver = CaregiverBuilder.transform(updatedCaregiver);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was updated in db", caregiver.getId());
        return caregiver.getId();
    }

    public CaregiverDTO findCaregiverByEmail(String email){
        List<CaregiverDTO> caregivers=this.findCaregivers();
        if(caregivers!=null) {
            for (CaregiverDTO caregiver : caregivers) {
                if (caregiver.getEmail().equals(email)){
                    return caregiver;
                }
            }
        }
        return null;
    }


}
