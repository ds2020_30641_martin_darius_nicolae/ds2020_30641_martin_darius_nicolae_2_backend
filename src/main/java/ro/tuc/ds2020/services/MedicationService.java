package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID id) {
        Optional<Medication> medication = medicationRepository.findById(id);
        if (!medication.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(medication.get());
    }

    public MedicationDTO findMedicationByName(String name) {
        List<Medication> list = medicationRepository.findByName(name);
        if (list.isEmpty()) {
            LOGGER.error("Medication with name {} was not found in db", name);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with name: " + name);
        }
        return MedicationBuilder.toMedicationDTO(list.get(0));
    }

    public UUID insert(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId_medication());
        return medication.getId_medication();
    }

    public UUID delete(String name){
        List<Medication>  listOfMedications=medicationRepository.findByName(name);
        medicationRepository.delete(listOfMedications.get(0));
        return listOfMedications.get(0).getId_medication();
    }

    public UUID update(MedicationDTO updatedMedication){
        Medication medication = MedicationBuilder.transform(updatedMedication);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was updated in db", medication.getId_medication());
        return medication.getId_medication();
    }

}