package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class MedicationPlan  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id_medicationPlan;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "intakeIntervals", nullable = false)
    private String intakeIntervals;

    @Column(name = "period", nullable = false)
    private String period;

    /*
    @ManyToMany(fetch=FetchType.LAZY, cascade={CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name="medication_medicationPlan",
               joinColumns = {@JoinColumn(name="id_medicationPlan")},
               inverseJoinColumns = {@JoinColumn(name="id_medication")}
    )
    */

    @ManyToMany(fetch = FetchType.EAGER,cascade=CascadeType.DETACH)
    @JoinColumn(name = "id_medication")
    private List<Medication> listOfMedications=new ArrayList<Medication>();


    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.DETACH)
    @JoinColumn(name = "id_patient")
    private Patient patient;


    public MedicationPlan() {
    }

    public MedicationPlan(String name,String intakeIntervals,String period,List<Medication> listOfMedications,Patient patient) {
        this.name=name;
        this.intakeIntervals = intakeIntervals;
        this.period=period;
        this.listOfMedications=listOfMedications;
        this.patient=patient;
    }

    public MedicationPlan(UUID id,String name,String intakeIntervals,String period,List<Medication> listOfMedications,Patient patient){
        this.id_medicationPlan=id;
        this.name=name;
        this.intakeIntervals = intakeIntervals;
        this.period=period;
        this.listOfMedications=listOfMedications;
        this.patient=patient;
    }

    public UUID getId_medicationPlan() {
        return id_medicationPlan;
    }

    public void setId_medicationPlan(UUID id_medicationPlan) {
        this.id_medicationPlan = id_medicationPlan;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public List<Medication> getListOfMedications() {
        return listOfMedications;
    }

    public void setListOfMedications(List<Medication> listOfMedications) {
        this.listOfMedications = listOfMedications;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}

