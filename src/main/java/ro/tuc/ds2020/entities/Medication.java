package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Medication  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id_medication;


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sideEffects", nullable = false)
    private String sideEffects;

    @Column(name = "dosage", nullable = false)
    private String dosage;

    /*
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "Medication")
    private List<MedicationPlan> listOfMedicationPlans = new ArrayList<>();
    */

    public Medication() {
    }

    public Medication(String name,String sideEffects,String dosage) {
        this.name = name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
    }

    public Medication(UUID id_medication, String name, String sideEffects, String dosage) {
        this.id_medication = id_medication;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
      //  this.listOfMedicationPlans=listOfMedicationPlans;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId_medication() {
        return id_medication;
    }

    public void setId_medication(UUID id_medication) {
        this.id_medication = id_medication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

}