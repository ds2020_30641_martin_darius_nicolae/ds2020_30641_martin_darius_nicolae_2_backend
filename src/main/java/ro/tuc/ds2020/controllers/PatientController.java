package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public PatientController(PatientService patientService, MedicationPlanService medicationPlanService) {
        this.patientService = patientService;
        this.medicationPlanService=medicationPlanService;
    }

    @GetMapping("/allPatients")
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatients();
        for (PatientDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatient(dto.getId())).withRel("patientDetails");
            dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDTO patientDTO) {
        UUID patientID = patientService.insert(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("id") UUID patientId) {
        PatientDTO dto = patientService.findPatientById(patientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

    @DeleteMapping("/delete/{name}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("name") String patientName) {
        List<MedicationPlanDTO> medications= medicationPlanService.findMedicationPlans();
        for(MedicationPlanDTO medic: medications){
            if(medic.getPatientDTO()!=null){
                if(medic.getPatientDTO().getName().equals(patientName)){
                    medic.setPatientDTO(null);
                    medicationPlanService.update(medic);
                }
            }
        }
        UUID id=patientService.delete(patientName);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("/update/{name}")
    public ResponseEntity<UUID> updatePatient(@PathVariable("name") String patientName,@Valid @RequestBody PatientDTO updatedPatient) {
        PatientDTO patientDTO=patientService.findPatientByName(patientName);
        patientDTO=patientService.findPatientById(patientDTO.getId());
        patientDTO.setAddress(updatedPatient.getAddress());
        patientDTO.setName(updatedPatient.getName());
        patientDTO.setGender(updatedPatient.getGender());
        patientDTO.setBirthDate(updatedPatient.getBirthDate());
        patientDTO.setEmail(updatedPatient.getEmail());
        patientDTO.setPassword(updatedPatient.getPassword());
        patientDTO.setMedicalRecord(updatedPatient.getMedicalRecord());
        UUID id=patientService.update(patientDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


    @GetMapping("/AllMedicationPlans/{name}")
    public List<MedicationPlanDTO> allMedicationPlans(@PathVariable("name") String patientName){
        PatientDTO patient=patientService.findPatientByName(patientName);
        List<MedicationPlanDTO> medicationPlans= medicationPlanService.findMedicationPlans();
        List<MedicationPlanDTO> medPlans2=new ArrayList<MedicationPlanDTO>();
        for(int i=0;i<medicationPlans.size();i++) {
            MedicationPlanDTO med=medicationPlans.get(i);
            if (med.getPatientDTO() != null) {
                if (med.getPatientDTO().getId().equals(patient.getId())) {
                    medPlans2.add(med);
                }
            }
        }
        return medPlans2;
    }

    @GetMapping("/getPatient/{name}")
    public ResponseEntity<PatientDTO> getPatientByName(@PathVariable("name") String patientName) {
       PatientDTO patient=patientService.findPatientByName(patientName);
       return new ResponseEntity<>(patient, HttpStatus.OK);
    }

}
